import asyncio
import websockets
import json


connections = set()

jrpc_error_message = json.dumps({
    "jsonrpc": "2.0",
    "error": {"code": -1, "message": "Invalid Request"},
    "id": None
    }, ensure_ascii=False)

def jrpc_response(message, request_id):
    return json.dumps({
        "jsonrpc": "2.0",
        "result": message, 
        "id": request_id
    }, ensure_ascii=False)

async def send_message(conn, request_id,  message):
    message = jrpc_response(message, request_id)
    await asyncio.wait([ws.send(message) for ws in connections])
    await asyncio.sleep(3)

async def send_echo(conn, request_id, message):
    await conn.send(jrpc_response(message, request_id))
    
commands = {
    'sendMessage': send_message,
    'sendEcho': send_echo
}

async def client_handle(websocket, path):
    connections.add(websocket)
    try:
        async for message in websocket:
            jrpc_request = json.loads(message)
            await commands[jrpc_request['method']](
                websocket,
                jrpc_request["id"],
                jrpc_request['params']['message']
            )
    except:
        await websocket.send(jrpc_error_message)
    finally:
        connections.remove(websocket)

async def echo_server():
    server = await websockets.serve(client_handle, '0.0.0.0', 8765)
    await server.wait_closed()

loop = asyncio.get_event_loop()
loop.run_until_complete(echo_server())
