# Асинхронный websocket сервер


## Установка пакетов и запуск сервера

```bash
pip3 install -r requirements.txt
python3 server.py
```
Рекомендуется использовать venv

## Установка и запуск через Docker

```bash
docker build --tag python3-websocoket-server .
docker run -d -p 8765:8765 python3-websocoket-server
```

## JRPC запросы

### Отправить сообщение всем

```bash
{ 
    "jsonrpc": "2.0",
    "method": "sendMessage", 
    "params": { "ids": "*", "message": "Всем привет" },
    "id": 3
 }
```

### Эхо сообщение

```bash
{ 
    "jsonrpc": "2.0", 
    "method": "sendEcho",
    "params": { "message": "Сообщение себе" },
    "id": 4 
}
```
